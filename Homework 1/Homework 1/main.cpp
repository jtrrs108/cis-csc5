//
//  main.cpp
//  Homework 1
//
//  Created by Jorge Torres on 9/19/20.
//  Copyright © 2020 Jorge Torres. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
   
    
    
    
   // Problem 1
    std:: cout << "Problem 1" << std:: endl;
    int x = 50;
    int y = 100;
    std:: cout << "x = 50" << std:: endl << "y = 100" << std:: endl;

    int total = x + y;
    
    std:: cout << " x + Y = " << total << std:: endl << std:: endl;
    
    
    // Problem 2
    std:: cout << "Problem 2" << std:: endl;
    int v = 95 ;
    double t = .04;
    double u = .02;
    
    std:: cout << "Total: " << v << std:: endl;
    std:: cout << "state tax: " << t * 100 << " percent" << std:: endl;
    std:: cout << "county tax: " << u * 100 << " percent" << std:: endl;
    std:: cout << "total sales tax: " << v * ( t+u)<< std:: endl << std:: endl;
    
    
    // Problem 3
    std:: cout << "Problem 3" << std:: endl;
    double mealCost = 88.67;
    double tax= .0675;
    double tip = .2;
    double a = mealCost * tax;
    double b = mealCost + a;
    double c = b * tip;
    double totalAfterTaxAndTip = b + c;
    std:: cout << "Meal cost: " << mealCost << std:: endl;
    std:: cout << "Tax amount: " << tax * 100 << " percent" << std:: endl;
    std:: cout << "Tip amount: " << tip * 100 << " percent" << std:: endl;
    std:: cout << "Total bill: " << totalAfterTaxAndTip << std:: endl << std:: endl;
   
    
    //Problem 4
    std:: cout << "Problem 4" << std:: endl;
    int gallons = 15;
    int miles = 375;
    int mpg = miles / gallons;
    
    std:: cout << "Gallons: " << gallons << std:: endl;
    std:: cout << "Miles: " << miles << std:: endl;
    std:: cout << "Miles per gallon: " << mpg << std:: endl << std:: endl;
    
    
   // Problem 5
    std:: cout << "Problem 5" << std:: endl;
    
    std:: cout << "Name: Jorge Torres" << std:: endl << "Phone Number: 5625555555" << std:: endl << "Major: Mathematics" << std:: endl << std:: endl;
    
    
    // Problem 6
    
    std:: cout << "Hello\n";
    std:: cout << "Problem 6" << std:: endl;
    
    int number_of_pods, peas_per_pod, total_peas;
    std:: cout << "Press return after entring a number.\n";
    std:: cout << "Enter the number of pods: \n";
    
    std:: cin >> number_of_pods;
    
    std:: cout << "Enter the number of peas in a pod: \n";
    std:: cin >> peas_per_pod;
    total_peas = number_of_pods + peas_per_pod;
    std:: cout << "If you have ";
    std:: cout << number_of_pods;
    std:: cout << " pea pods\n";
    std:: cout << "and ";
    std:: cout << peas_per_pod;
    std:: cout << " peas in each pod, then\n";
    std:: cout << "you have ";
    std:: cout << total_peas;
    std:: cout << " peas in all the pods.\n";
    
    std:: cout << "Good-bye\n";
    
    return 0;
}



